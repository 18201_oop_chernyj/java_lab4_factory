import factory.CarFactory;
import factory.CarFactoryPropertiesLoader;
import factory.Dealer;
import factory.Suppliers.AccessorySupplier;
import factory.Suppliers.BodySupplier;
import factory.Suppliers.MotorSupplier;
import factory.View.Graphics;
import factory.products.Car;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {
    public static void main(String[] args) {
        final String configFileName = "config.properties";
        final String logFileName = "CarFactory.log";
        Logger log = Logger.getLogger(Main.class.getName());
        CarFactoryPropertiesLoader propertiesLoader = new CarFactoryPropertiesLoader(configFileName);
        propertiesLoader.loadProperties();
        log.setUseParentHandlers(propertiesLoader.isLogSaleToSystemOut());
        if (propertiesLoader.isLogOn()) {
            try {
                FileHandler fileHandler = new FileHandler(logFileName);
                log.addHandler(fileHandler);
                SimpleFormatter simpleFormatter = new SimpleFormatter();
                fileHandler.setFormatter(simpleFormatter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            log.setLevel(Level.OFF);

        CarFactory factory = new CarFactory(propertiesLoader);

        ArrayList<Dealer<Car>> dealers = new ArrayList<Dealer<Car>>();
        for (int i = 0; i < propertiesLoader.getDealerCount(); i++) {
            dealers.add(new Dealer<Car>(factory.getCarStorage(), propertiesLoader.getDealerTimer()));
        }

        ArrayList<AccessorySupplier> accessorySuppliers = new ArrayList<AccessorySupplier>();
        for (int i = 0; i < propertiesLoader.getAccessorySupplierCount(); i++) {
            accessorySuppliers.add(new AccessorySupplier(factory.getAccessoryStorage(), propertiesLoader.getAccessorySupplierTimer()));
        }

        BodySupplier bodySupplier = new BodySupplier(factory.getBodyStorage(), propertiesLoader.getBodySupplierTimer());
        MotorSupplier motorSupplier = new MotorSupplier(factory.getMotorStorage(), propertiesLoader.getMotorSupplierTimer());

        Graphics graphics = new Graphics(factory);
        factory.startFactory();
        for (Dealer<Car> dealer : dealers) {
            dealer.start();
        }
        for (AccessorySupplier supplier : accessorySuppliers) {
            supplier.start();
        }
        bodySupplier.start();
        motorSupplier.start();
        graphics.createAndShowGUI();
    }
}
