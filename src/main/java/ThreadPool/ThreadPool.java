package ThreadPool;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Stack;

public class ThreadPool<Task extends ThreadTask, Worker extends ThreadWorker> {
    Class<Worker> workerClass;
    private int workerCount;
    private Stack<Task> tasks;
    private ArrayList<Worker> workers;

    public ThreadPool(int workerCount, Stack<Task> tasks, Class<Worker> workerClass, long timer) {
        this.workerCount = workerCount;
        this.tasks = tasks;
        workers = new ArrayList<>();
        for (int i = 0; i < workerCount; i++) {
            try {
                workers.add(workerClass.getDeclaredConstructor(tasks.getClass(), int.class, long.class).newInstance(tasks, i, timer));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Worker> getWorkers() {
        return workers;
    }

    public void start() {
        for (Worker worker : workers) {
            worker.start();
        }
    }
}
