package ThreadPool;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.Stack;

public abstract class ThreadWorker<Task extends ThreadTask> extends Thread {
    public static final String STATUS = "status";
    protected Stack<Task> tasks;
    protected int workerId;
    protected SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);
    protected int status;

    public int getWorkerId() {
        return workerId;
    }

    /*
        public ThreadWorker(Stack<CarTask> tasks, int id, long timer) {
        }
    */
    public abstract void doTask(Task task);

    public abstract String printStatus();

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            synchronized (tasks) {
                try {
                    tasks.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (!tasks.empty()) {
                doTask(tasks.pop());
            }
        }
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }

    public void setStatus(int newStatus) {
        int oldValue = status;
        status = newStatus;
        int newValue = status;
        pcSupport.firePropertyChange(STATUS, oldValue, newValue);
    }
}
