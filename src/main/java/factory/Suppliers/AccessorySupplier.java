package factory.Suppliers;

import factory.Storage;
import factory.Supplier;
import factory.products.Accessory;

public class AccessorySupplier extends Supplier<Accessory> {
    long timer;
    Storage<Accessory> storage;

    public AccessorySupplier(Storage<Accessory> storage, long timer) {
        this.timer = timer;
        this.storage = storage;
    }

    @Override
    public void run() {
        Accessory accessory = new Accessory();
        synchronized (this) {
            while (!this.isInterrupted()) {
                storage.addProduct(accessory);
                try {
                    wait(timer);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
