package factory.Suppliers;

import factory.Storage;
import factory.Supplier;
import factory.products.Motor;

public class MotorSupplier extends Supplier<Motor> {
    long timer;
    Storage<Motor> storage;

    public MotorSupplier(Storage<Motor> storage, long timer) {
        this.timer = timer;
        this.storage = storage;
    }

    @Override
    public void run() {
        Motor motor = new Motor();
        synchronized (this) {
            while (!this.isInterrupted()) {
                storage.addProduct(motor);
                try {
                    wait(timer);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
