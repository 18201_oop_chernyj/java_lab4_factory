package factory.Suppliers;

import factory.Storage;
import factory.Supplier;
import factory.products.Body;

public class BodySupplier extends Supplier<Body> {
    long timer;
    Storage<Body> storage;

    public BodySupplier(Storage<Body> storage, long timer) {
        this.timer = timer;
        this.storage = storage;
    }

    @Override
    public void run() {
        Body body = new Body();
        synchronized (this) {
            while (!this.isInterrupted()) {
                storage.addProduct(body);
                try {
                    wait(timer);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
