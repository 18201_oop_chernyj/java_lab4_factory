package factory;

import ThreadPool.ThreadTask;
import factory.products.Accessory;
import factory.products.Body;
import factory.products.Car;
import factory.products.Motor;

public class CarTask extends ThreadTask {
    Storage<Accessory> accessoryStorage;
    Storage<Body> bodyStorage;
    Storage<Motor> motorStorage;
    Storage<Car> carStorage;
    CarWorker worker;

    public CarTask(Storage<Accessory> accessoryStorage, Storage<Body> bodyStorage, Storage<Motor> motorStorage, Storage<Car> carStorage) {
        this.accessoryStorage = accessoryStorage;
        this.bodyStorage = bodyStorage;
        this.motorStorage = motorStorage;
        this.carStorage = carStorage;
    }

    public Storage<Accessory> getAccessoryStorage() {
        return accessoryStorage;
    }

    public void setAccessoryStorage(Storage<Accessory> accessoryStorage) {
        this.accessoryStorage = accessoryStorage;
    }

    public Storage<Body> getBodyStorage() {
        return bodyStorage;
    }

    public void setBodyStorage(Storage<Body> bodyStorage) {
        this.bodyStorage = bodyStorage;
    }

    public Storage<Motor> getMotorStorage() {
        return motorStorage;
    }

    public void setMotorStorage(Storage<Motor> motorStorage) {
        this.motorStorage = motorStorage;
    }

    public Storage<Car> getCarStorage() {
        return carStorage;
    }

    public void setCarStorage(Storage<Car> carStorage) {
        this.carStorage = carStorage;
    }

    public CarWorker getWorker() {
        return worker;
    }

    public void setWorker(CarWorker worker) {
        this.worker = worker;
    }
}
