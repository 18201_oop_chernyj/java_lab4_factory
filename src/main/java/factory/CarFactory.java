package factory;

import ThreadPool.ThreadPool;
import factory.products.Accessory;
import factory.products.Body;
import factory.products.Car;
import factory.products.Motor;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.Stack;

public class CarFactory extends Factory {
    public final static String CARS_SOLD = "cars sold";
    Storage<Accessory> accessoryStorage;
    Storage<Body> bodyStorage;
    Storage<Motor> motorStorage;
    Storage<Car> carStorage;
    ThreadPool<CarTask, CarWorker> workerPool;
    StorageController controller;
    Stack<CarTask> tasks;
    private SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);
    private Integer carsSold;

    public CarFactory(CarFactoryPropertiesLoader propertiesLoader) {
        carsSold = 0;
        accessoryStorage = new Storage<Accessory>(propertiesLoader.getStorageAccessorySize());
        bodyStorage = new Storage<Body>(propertiesLoader.getStorageBodySize());
        motorStorage = new Storage<Motor>(propertiesLoader.getStorageMotorSize());
        carStorage = new Storage<Car>(propertiesLoader.getStorageCarSize());
        tasks = new Stack<CarTask>();
        controller = new StorageController(this, carStorage);
        workerPool = new ThreadPool<CarTask, CarWorker>(propertiesLoader.getWorkerCount(), tasks, CarWorker.class, propertiesLoader.getWorkerTimer());
    }

    public CarFactory(int workersCount, long timer, int storageCapacity) {
        carsSold = 0;
        accessoryStorage = new Storage<Accessory>(storageCapacity);
        bodyStorage = new Storage<Body>(storageCapacity);
        motorStorage = new Storage<Motor>(storageCapacity);
        carStorage = new Storage<Car>(storageCapacity);
        tasks = new Stack<CarTask>();
        controller = new StorageController(this, carStorage);
        workerPool = new ThreadPool<CarTask, CarWorker>(workersCount, tasks, CarWorker.class, timer);
    }

    public Integer getCarsSold() {
        return carsSold;
    }

    public void setCarsSold(int carsSold) {
        this.carsSold = carsSold;
    }

    public int getTaskCount() {
        return tasks.size();
    }

    public void createTask() {
        synchronized (tasks) {
            tasks.add(new CarTask(accessoryStorage, bodyStorage, motorStorage, carStorage));
            tasks.notify();
        }
    }

    public void startFactory() {
        workerPool.start();
        controller.start();
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }

    public void incCarsSold() {
        int oldValue = carsSold;
        carsSold++;
        int newValue = carsSold;
        pcSupport.firePropertyChange(CARS_SOLD, oldValue, newValue);
    }

    public Storage<Accessory> getAccessoryStorage() {
        return accessoryStorage;
    }

    public void setAccessoryStorage(Storage<Accessory> accessoryStorage) {
        this.accessoryStorage = accessoryStorage;
    }

    public Storage<Body> getBodyStorage() {
        return bodyStorage;
    }

    public void setBodyStorage(Storage<Body> bodyStorage) {
        this.bodyStorage = bodyStorage;
    }

    public Storage<Motor> getMotorStorage() {
        return motorStorage;
    }

    public void setMotorStorage(Storage<Motor> motorStorage) {
        this.motorStorage = motorStorage;
    }

    public Storage<Car> getCarStorage() {
        return carStorage;
    }

    public void setCarStorage(Storage<Car> carStorage) {
        this.carStorage = carStorage;
    }

    public ThreadPool getWorkerPool() {
        return workerPool;
    }

    public void setWorkerPool(ThreadPool workerPool) {
        this.workerPool = workerPool;
    }

    public StorageController getController() {
        return controller;
    }

    public void setController(StorageController controller) {
        this.controller = controller;
    }

    public Stack<CarTask> getTasks() {
        return tasks;
    }

    public void setTasks(Stack<CarTask> tasks) {
        this.tasks = tasks;
    }
}
