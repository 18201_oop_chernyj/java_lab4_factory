package factory;

import ThreadPool.ThreadWorker;
import factory.products.Accessory;
import factory.products.Body;
import factory.products.Car;
import factory.products.Motor;

import java.util.Stack;

public class CarWorker extends ThreadWorker<CarTask> {
    public static final int STATUS_SLEEPING = 0;
    public static final int STATUS_WAITING = 1;
    public static final int STATUS_WORKING = 2;
    private static final String STATUS_STRING_SLEEPING = "Sleeping";
    private static final String STATUS_STRING_WAITING = "Waiting";
    private static final String STATUS_STRING_WORKING = "Working";

    //private Stack<CarTask> tasks;
    private long timer;
    // private int status;
    //private int id;

    public CarWorker(Stack<CarTask> tasks, int id, long timer) {
        this.tasks = tasks;
        this.workerId = id;
        this.timer = timer;
        status = STATUS_SLEEPING;
    }

    public void doTask(CarTask task) {
        setStatus(STATUS_WAITING);
        Accessory accessory = task.getAccessoryStorage().getProduct();
        Body body = task.getBodyStorage().getProduct();
        Motor motor = task.getMotorStorage().getProduct();
        setStatus(STATUS_WORKING);
        synchronized (this) {
            try {
                wait(timer);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Car newCar = new Car(accessory, body, motor);
        setStatus(STATUS_WAITING);
        synchronized (task.getCarStorage()) {
            task.getCarStorage().addProduct(newCar);
            task.getCarStorage().notify();
        }
        setStatus(STATUS_SLEEPING);
    }


    public String printStatus() {
        switch (status) {
            case STATUS_WAITING:
                return STATUS_STRING_WAITING;
            case STATUS_WORKING:
                return STATUS_STRING_WORKING;
            case STATUS_SLEEPING:
                return STATUS_STRING_SLEEPING;
            default:
                return "";
        }
    }
}
