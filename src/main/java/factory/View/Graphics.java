package factory.View;

import ThreadPool.ThreadWorker;
import factory.CarFactory;
import factory.Storage;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Graphics extends JFrame {
    private final String WINDOW_TITLE = "Factory";
    private final String ACCESSORY_STORAGE_LABEL = "Accessories: ";
    private final String BODY_STORAGE_LABEL = "Bodies: ";
    private final String MOTOR_STORAGE_LABEL = "Motors: ";
    private final String CAR_STORAGE_LABEL = "Cars: ";
    private final String WORKER_LABEL = "Worker ";
    private final String CARS_SOLD_LABEL = "Cars sold: ";

    private final int STORAGE_COUNT = 4;
    private final int ADDITIONAL_LINES_COUNT = 1;
    private final int WINDOW_SIZE_X = 800;
    private final int WINDOW_SIZE_Y = 600;
    private final int DEFAULT_PANEL_DISTANCE = 10;
    CarFactory factory;
    JProgressBar accessoryStorageBar;
    JProgressBar bodyStorageBar;
    JProgressBar motorStorageBar;
    JProgressBar carStorageBar;
    JLabel accessoryLabel;
    JLabel bodyLabel;
    JLabel motorLabel;
    JLabel carLabel;
    JLabel carsSoldLabel;
    Map<Integer, JLabel> workerNameLabels;
    Map<Integer, JLabel> workerStatusLabels;
    JFrame frame;

    public Graphics (final CarFactory factory){
        this.factory = factory;
        accessoryStorageBar = new JProgressBar(0, factory.getAccessoryStorage().getCapacity());
        bodyStorageBar = new JProgressBar(0, factory.getBodyStorage().getCapacity());
        motorStorageBar = new JProgressBar(0, factory.getMotorStorage().getCapacity());
        carStorageBar = new JProgressBar(0, factory.getCarStorage().getCapacity());

         accessoryLabel = new JLabel(ACCESSORY_STORAGE_LABEL);
         bodyLabel = new JLabel(BODY_STORAGE_LABEL);
         motorLabel = new JLabel(MOTOR_STORAGE_LABEL);
         carLabel = new JLabel(CAR_STORAGE_LABEL);
         carsSoldLabel = new JLabel(CARS_SOLD_LABEL + factory.getCarsSold());

        factory.getAccessoryStorage().addPropertyChangeListener(Storage.PRODUCTS, pcEvent -> setProgress(accessoryStorageBar, accessoryLabel, ACCESSORY_STORAGE_LABEL, factory.getAccessoryStorage()));
        factory.getBodyStorage().addPropertyChangeListener(Storage.PRODUCTS, pcEvent -> setProgress(bodyStorageBar, bodyLabel, BODY_STORAGE_LABEL, factory.getBodyStorage()));
        factory.getMotorStorage().addPropertyChangeListener(Storage.PRODUCTS, pcEvent -> setProgress(motorStorageBar, motorLabel, MOTOR_STORAGE_LABEL, factory.getMotorStorage()));
        factory.getCarStorage().addPropertyChangeListener(Storage.PRODUCTS, pcEvent -> setProgress(carStorageBar, carLabel, CAR_STORAGE_LABEL, factory.getCarStorage()));
        factory.addPropertyChangeListener(CarFactory.CARS_SOLD,  pcEvent -> setCarsSold(carsSoldLabel, factory.getCarsSold()));

        ArrayList<ThreadWorker> workers = factory.getWorkerPool().getWorkers();
        workerNameLabels = new HashMap<Integer, JLabel>();
        workerStatusLabels = new HashMap<Integer, JLabel>();
        for (ThreadWorker worker: workers) {
            workerNameLabels.put(worker.getWorkerId(), new JLabel(WORKER_LABEL + worker.getWorkerId() + ":"));
            workerStatusLabels.put(worker.getWorkerId(), new JLabel());
            worker.addPropertyChangeListener(ThreadWorker.STATUS, pcEvent -> setWorkerStatus(workerStatusLabels.get(worker.getWorkerId()), worker));
        }

        accessoryStorageBar.setValue(factory.getAccessoryStorage().getProductCount());
        bodyStorageBar.setValue(factory.getBodyStorage().getProductCount());
        motorStorageBar.setValue(factory.getMotorStorage().getProductCount());
        carStorageBar.setValue(factory.getCarStorage().getProductCount());
    }

    public void createAndShowGUI() {
        frame = new JFrame(WINDOW_TITLE);
        frame.setPreferredSize(new Dimension(WINDOW_SIZE_X, WINDOW_SIZE_Y));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpringLayout layout = new SpringLayout();
        JPanel panel = new JPanel();
        panel.setLayout(layout);
        accessoryStorageBar.setStringPainted(true);
        bodyStorageBar.setStringPainted(true);
        motorStorageBar.setStringPainted(true);
        carStorageBar.setStringPainted(true);

        //Creating layout
        panel.add(accessoryLabel);
        panel.add(accessoryStorageBar);
        layout.putConstraint(SpringLayout.WEST, accessoryLabel, DEFAULT_PANEL_DISTANCE, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, accessoryLabel, DEFAULT_PANEL_DISTANCE, SpringLayout.NORTH, panel);
        layout.putConstraint(SpringLayout.WEST, accessoryStorageBar, DEFAULT_PANEL_DISTANCE, SpringLayout.EAST, accessoryLabel);
        layout.putConstraint(SpringLayout.NORTH, accessoryStorageBar, 0, SpringLayout.NORTH, accessoryLabel);
        layout.putConstraint(SpringLayout.EAST, accessoryStorageBar, -1 * DEFAULT_PANEL_DISTANCE, SpringLayout.EAST, panel);

        panel.add(bodyLabel);
        panel.add(bodyStorageBar);
        putMiddleConstraints(panel, layout, bodyLabel, bodyStorageBar, accessoryLabel, accessoryStorageBar);

        panel.add(motorLabel);
        panel.add(motorStorageBar);
        putMiddleConstraints(panel, layout, motorLabel, motorStorageBar, bodyLabel, bodyStorageBar);

        panel.add(carLabel);
        panel.add(carStorageBar);
        putMiddleConstraints(panel, layout, carLabel, carStorageBar, motorLabel, motorStorageBar);

        JComponent upperWestComponent = carLabel,  upperEastComponent = carStorageBar;
        for (Map.Entry<Integer, JLabel> workerLabel: workerNameLabels.entrySet()) {
            panel.add(workerLabel.getValue());
            panel.add(workerStatusLabels.get(workerLabel.getKey()));
            putMiddleConstraints(panel, layout, workerLabel.getValue(), workerStatusLabels.get(workerLabel.getKey()), upperWestComponent, upperEastComponent);
            upperWestComponent = workerLabel.getValue();
            upperEastComponent = workerStatusLabels.get(workerLabel.getKey());
        }

        panel.add(carsSoldLabel);
        layout.putConstraint(SpringLayout.WEST, carsSoldLabel, DEFAULT_PANEL_DISTANCE, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, carsSoldLabel, DEFAULT_PANEL_DISTANCE, SpringLayout.SOUTH, upperWestComponent);
        layout.putConstraint(SpringLayout.SOUTH, carsSoldLabel, DEFAULT_PANEL_DISTANCE, SpringLayout.SOUTH, panel);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    private void putMiddleConstraints(JPanel panel, SpringLayout layout, JComponent westComponent, JComponent eastComponent, JComponent upperWestComponent, JComponent upperEastComponent){
        layout.putConstraint(SpringLayout.WEST, westComponent, DEFAULT_PANEL_DISTANCE, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, westComponent, DEFAULT_PANEL_DISTANCE, SpringLayout.SOUTH, upperWestComponent);
        layout.putConstraint(SpringLayout.WEST, eastComponent, 0, SpringLayout.WEST, upperEastComponent);
        layout.putConstraint(SpringLayout.NORTH, eastComponent, 0, SpringLayout.NORTH, westComponent);
        layout.putConstraint(SpringLayout.EAST, eastComponent, -DEFAULT_PANEL_DISTANCE, SpringLayout.EAST, panel);
    }

    private void setProgress(JProgressBar progressBar, JLabel label, String labelText, Storage storage)
    {
        progressBar.setValue(storage.getProductCount());
        label.setText(labelText + storage.getProductCount() + "/" + storage.getCapacity());
    }

    private void setWorkerStatus(JLabel label, ThreadWorker worker){
        label.setText(worker.printStatus());
    }

    private void setCarsSold(JLabel label, int carsSold) {
        label.setText(CARS_SOLD_LABEL + carsSold);
    }
}
