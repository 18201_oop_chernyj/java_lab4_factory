package factory;

public abstract class Factory {
    public abstract void createTask();

    public abstract void incCarsSold();
}
