package factory.products;

public class Accessory extends Product {
    public String printInfo() {
        return ("Accessory " + this.id);
    }
}
