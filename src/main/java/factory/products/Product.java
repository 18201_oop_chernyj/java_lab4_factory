package factory.products;

public abstract class Product {
    static protected int idCounter = 0;
    protected int id;

    public Product() {
        this.id = idCounter;
        idCounter++;
    }

    public abstract String printInfo();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
