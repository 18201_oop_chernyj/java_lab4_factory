package factory.products;

public class Motor extends Product {
    public String printInfo() {
        return ("Motor " + this.id);
    }
}
