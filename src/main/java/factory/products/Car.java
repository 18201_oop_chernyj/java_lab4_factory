package factory.products;

public class Car extends Product {
    Accessory accessory;
    Body body;
    Motor motor;

    public Car(Accessory accessory, Body body, Motor motor) {
        this.id = idCounter;
        idCounter++;
        this.accessory = accessory;
        this.body = body;
        this.motor = motor;
    }

    public String printInfo() {
        return ("Car " + this.id + "(" + body.printInfo() + ", " + motor.printInfo() + ", " + accessory.printInfo() + ")");
    }
}
