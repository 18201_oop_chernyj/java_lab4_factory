package factory;

import factory.products.Product;

import java.util.logging.Logger;

public class Dealer<T extends Product> extends Thread {
    long timer;
    int id;
    Storage<T> storage;
    Logger dealerLogger;

    public Dealer(Storage<T> storage, long timer) {
        this.timer = timer;
        this.storage = storage;
        dealerLogger = Logger.getLogger(Dealer.class.getName());
        dealerLogger.setParent(Logger.getLogger("Main"));
    }

    @Override
    public void run() {
        T newProduct;
        while (!this.isInterrupted()) {
            newProduct = storage.getProduct();
            synchronized (this) {
                try {
                    wait(timer);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dealerLogger.info("Dealer " + id + ": " + newProduct.printInfo());
            }
        }
    }
}
