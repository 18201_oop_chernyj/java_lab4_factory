package factory;

import factory.products.Product;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.Stack;

public class Storage<ProductType extends Product> {
    public static final String PRODUCTS = "products";
    int capacity;
    Stack<ProductType> products;
    private SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);

    public Storage(int capacity) {
        this.capacity = capacity;
        products = new Stack<>();
    }

    public boolean isFull() {
        return (products.size() >= capacity);
    }

    public boolean isEmpty() {
        return (products.size() == 0);
    }

    public int getProductCount() {
        return products.size();
    }

    public int getCapacity() {
        return capacity;
    }

    public void addProduct(ProductType product) {
        synchronized (products) {
            while (isFull())
                try {
                    products.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            int oldValue = products.size();
            products.add(product);
            int newValue = products.size();
            products.notify();
            pcSupport.firePropertyChange(PRODUCTS, oldValue, newValue);
        }
    }

    public ProductType getProduct() {
        ProductType product = null;
        synchronized (products) {
            while (isEmpty())
                try {
                    products.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            int oldValue = products.size();
            product = products.pop();
            int newValue = products.size();
            products.notify();
            pcSupport.firePropertyChange(PRODUCTS, oldValue, newValue);
        }
        return product;
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }
}
