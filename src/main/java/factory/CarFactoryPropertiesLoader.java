package factory;

import ThreadPool.ThreadWorker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CarFactoryPropertiesLoader {
    private final String STORAGE_BODY_SIZE = "StorageBodySize";
    private final String STORAGE_MOTOR_SIZE = "StorageMotorSize";
    private final String STORAGE_ACCESSORY_SIZE = "StorageAccessorySize";
    private final String STORAGE_CAR_SIZE = "StorageCarSize";
    private final String ACCESSORY_SUPPLIER_COUNT = "AccessorySupplierCount";
    private final String ACCESSORY_SUPPLIER_TIMER = "AccessorySupplierTimer";
    private final String BODY_SUPPLIER_TIMER = "BodySupplierTimer";
    private final String MOTOR_SUPPLIER_TIMER = "MotorSupplierTimer";
    private final String WORKER_COUNT = "WorkerCount";
    private final String WORKER_TIMER = "WorkerTimer";
    private final String DEALER_COUNT = "DealerCount";
    private final String DEALER_TIMER = "DealerTimer";
    private final String LOG_ON = "LogSale";
    private final String LOG_TO_SYSTEM_OUT = "logSaleToSystemOut";
    InputStream inputStream;
    int StorageBodySize;
    int StorageMotorSize;
    int StorageAccessorySize;
    int storageCarSize;
    int accessorySupplierCount;
    long accessorySupplierTimer;
    long bodySupplierTimer;
    long motorSupplierTimer;
    int workerCount;
    long workerTimer;
    int dealerCount;
    long dealerTimer;
    boolean logOn;
    boolean logSaleToSystemOut;

    public CarFactoryPropertiesLoader(String propertiesFileName) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        inputStream = loader.getResourceAsStream(propertiesFileName);
    }

    public CarFactoryPropertiesLoader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public int getStorageMotorSize() {
        return StorageMotorSize;
    }

    public void setStorageMotorSize(int storageMotorSize) {
        StorageMotorSize = storageMotorSize;
    }

    public int getStorageBodySize() {
        return StorageBodySize;
    }

    public void setStorageBodySize(int storageBodySize) {
        StorageBodySize = storageBodySize;
    }

    public int getStorageAccessorySize() {
        return StorageAccessorySize;
    }

    public void setStorageAccessorySize(int storageAccessorySize) {
        StorageAccessorySize = storageAccessorySize;
    }

    public int getStorageCarSize() {
        return storageCarSize;
    }

    public void setStorageCarSize(int storageCarSize) {
        this.storageCarSize = storageCarSize;
    }

    public int getAccessorySupplierCount() {
        return accessorySupplierCount;
    }

    public void setAccessorySupplierCount(int accessorySupplierCount) {
        this.accessorySupplierCount = accessorySupplierCount;
    }

    public long getAccessorySupplierTimer() {
        return accessorySupplierTimer;
    }

    public void setAccessorySupplierTimer(long accessorySupplierTimer) {
        this.accessorySupplierTimer = accessorySupplierTimer;
    }

    public long getBodySupplierTimer() {
        return bodySupplierTimer;
    }

    public void setBodySupplierTimer(long bodySupplierTimer) {
        this.bodySupplierTimer = bodySupplierTimer;
    }

    public long getMotorSupplierTimer() {
        return motorSupplierTimer;
    }

    public void setMotorSupplierTimer(long motorSupplierTimer) {
        this.motorSupplierTimer = motorSupplierTimer;
    }

    public int getWorkerCount() {
        return workerCount;
    }

    public void setWorkerCount(int workerCount) {
        this.workerCount = workerCount;
    }

    public long getWorkerTimer() {
        return workerTimer;
    }

    public void setWorkerTimer(long workerTimer) {
        this.workerTimer = workerTimer;
    }

    public int getDealerCount() {
        return dealerCount;
    }

    public void setDealerCount(int dealerCount) {
        this.dealerCount = dealerCount;
    }

    public long getDealerTimer() {
        return dealerTimer;
    }

    public void setDealerTimer(long dealerTimer) {
        this.dealerTimer = dealerTimer;
    }

    public boolean isLogOn() {
        return logOn;
    }

    public void setLogOn(boolean logOn) {
        this.logOn = logOn;
    }

    public boolean isLogSaleToSystemOut() {
        return logSaleToSystemOut;
    }

    public void setLogSaleToSystemOut(boolean logSaleToSystemOut) {
        this.logSaleToSystemOut = logSaleToSystemOut;
    }

    public void loadProperties() {
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StorageBodySize = Integer.parseInt(properties.getProperty(STORAGE_BODY_SIZE));
        StorageMotorSize = Integer.parseInt(properties.getProperty(STORAGE_MOTOR_SIZE));
        StorageAccessorySize = Integer.parseInt(properties.getProperty(STORAGE_ACCESSORY_SIZE));
        storageCarSize = Integer.parseInt(properties.getProperty(STORAGE_CAR_SIZE));
        accessorySupplierCount = Integer.parseInt(properties.getProperty(ACCESSORY_SUPPLIER_COUNT));
        accessorySupplierTimer = Long.parseLong(properties.getProperty(ACCESSORY_SUPPLIER_TIMER));
        motorSupplierTimer = Long.parseLong(properties.getProperty(MOTOR_SUPPLIER_TIMER));
        bodySupplierTimer = Long.parseLong(properties.getProperty(BODY_SUPPLIER_TIMER));
        workerCount = Integer.parseInt(properties.getProperty(WORKER_COUNT));
        workerTimer = Long.parseLong(properties.getProperty(WORKER_TIMER));
        dealerCount = Integer.parseInt(properties.getProperty(DEALER_COUNT));
        dealerTimer = Long.parseLong(properties.getProperty(DEALER_TIMER));
        logOn = Boolean.parseBoolean(properties.getProperty(LOG_ON));
        logSaleToSystemOut = Boolean.parseBoolean(properties.getProperty(LOG_TO_SYSTEM_OUT));
    }
}
