package factory;

public class StorageController extends Thread {
    private Factory factory;
    private Storage storage;

    public StorageController(Factory factory, Storage storage) {
        this.factory = factory;
        this.storage = storage;
    }

    @Override
    public void run() {
        for (int i = storage.getProductCount(); i < storage.getCapacity(); i++) {
            factory.createTask();
        }

        while (!this.isInterrupted()) {
            synchronized (storage.products) {
                try {
                    storage.products.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            factory.createTask();
            factory.incCarsSold();
        }
    }
}
